libpathbuf
==========
A path buffer library inspired by the Rust [PathBuf](https://doc.rust-lang.org/std/path/struct.PathBuf.html).

This library supports both Windows and UNIX style paths. Path objects using this library are manipulated like a stack. To go down the directory tree, you call "path_push" with a filename on the path object. To go up the directory tree, you call "path_pop" on the path object.

There is CMake available, using add_subdirectory from the parent project will expose variables ${PATHBUF_SRCS} and ${PATHBUF_INCLUDE_DIRS} as well as library target pathbuf.

Included is a test suite for debugging. It is built by enabling option PATHBUF_BUILD_TESTS. Once built, the pathbuf_tester executable is available in the build directory. "run_test.sh" contains setup commands for test directories.

## Features
* Manipulate paths on both Windows/MinGW and Linux
* Manipulate relative and absolute paths
* Iterate directories
* Get, Change, or Remove a file extension with a single function call
* Create directories
* Check for file existence

## Basic example
```
  path_t* path = path_new_from_str("c:\\Users\\.\\bob\\..\\mary");
  path_push(&path, "ccc");

  if(path_exists(&path))
  {
    printf("Path %s exists\n", path_get_cstr(&path));
  }
  else
  {
    printf("Path %s does not exist\n", path_get_cstr(&path));
  }

  path_destroy(&path);
```
This will use various functions to create a path object pointing to "c:\\Users\\mary\\ccc" and print whether something exists there.
