/* Aeden McClain (c) 2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * license: MIT (see LICENSE.txt)
 *
 * This file is a part of libpathbuf.
 */

#ifndef INCLUDE_UTIL_PATHBUF_H
#define INCLUDE_UTIL_PATHBUF_H

#include <stdio.h>
#include <stdbool.h>

#ifdef _WIN32
  #include <windows.h>
  #include <shlwapi.h>
  #include <direct.h>
  #include <malloc.h>
  #include <stdio.h>
#else
  #include <unistd.h>
  #include <sys/stat.h>
  #include <sys/types.h>
  #include <dirent.h>
#endif

#define PATH_DEFAULT_ALLOC 64

typedef enum path_type_t_ {
  PATH_TYPE_UNIX,
  PATH_TYPE_UNIX_RELATIVE,
  PATH_TYPE_WINDOWS,
  PATH_TYPE_WINDOWS_RELATIVE,

} path_type_t;

struct path_header_t_;

typedef struct path_header_t_ path_t;
typedef path_t path_iterator_t;

#define PATH_FLAG_NO_PARENT (1 << 0)

/** Create a new path object from string, this function resolves all "." and ".." components.
 * @param src  The string source
 * @return  A new path object with the path set
 */
extern path_t* path_new_from_str(const char* src, unsigned int flags);

/** Create a new path object from current working directory.
 * @return  A new path object with the current working directory
 */
extern path_t* path_new_from_cwd(unsigned int flags);

/** Destroy a path object completely.
 * @param path  The path to destroy.
 */
extern void path_destroy(path_t** path);

/** Clone a path as a C String.
 * @param path  The path to spawn.
 * @return   A C String containing the path  which must be freed when finished with.
 */
extern char* path_spawn(path_t** path);

/** Downgrade a path to a C String. This destroys the path object.
 * @param path  The path to downgrade.
 * @return   A C String containing the path which must be freed when finished with.
 */
extern char* path_release(path_t** path);

/** Change a path object to an absolute one.
 * @param path  The path to change
 */
extern void path_make_absolute(
    path_t** path);

/** Compare two paths for equality
 * @param p1  The first path to compare
 * @param p2  The second path to compare
 * @return   Whether the paths are equal
 */
extern bool path_compare(path_t* p1, path_t* p2);

/** Clone a path object.
 * @param path  The path object to clone.
 * @return   The new path.
 */
extern path_t* path_clone(path_t** path);

/** Walk up the directory tree.
 * @param path  The path to walk up the tree.
 */
extern void   path_pop(path_t** path);

/** Walk one step down the directory tree. This function does ~not~ resolve "." and "..".
 * @param path  The path to walk
 * @param subpath The path to walk down
 * @return   The remaining subpath which has not been processed
 *           (as the subpath may have multiple levels).
 */
extern const char* path_push(path_t** path, const char* subpath);

/** Extend the current path without traversing down.
 * @param path  The path object to write to
 * @return      The text to append
 */
extern void path_cat(path_t** path, const char* txt);

/** Get a path file extension.
 * @param path  The path object to get the extension for.
 * @return   The path extension.
 */
extern char* path_extension_get(path_t** path);

/** Set a path file extension.
 * @param path  The path object to set the extension for.
 * @param path  The extension to set, NULL to remove extension.
 */
extern void path_extension_set(path_t** path, const char* extension);

/** Get the C-compatible path of a path object.
 * @param path  The path object
 * @return  The C-compatible path
 */
extern char*  path_get_cstr(path_t** path);

/** Get the basename of a path object.
 * @param path  The path object
 * @return  The basename
 */
extern char* path_get_basename(path_t** path);

/** Check if a path object points to a real file.
 * @param path  The path object
 * @return  Existence of the file
 */
extern bool   path_exists(path_t** path);

/** Check if a path object is at the filesystem root.
 * @param path  The path object
 * @return  Whether the path object is at the filesystem root.
 */
extern bool   path_is_root(path_t** path);

/** Check if a path object points to a directory.
 * @param path  The path object
 * @return  Whether the path object points to a directory
 */
extern bool   path_is_dir(path_t** path);

/** Create a directory pointed to by a path object.
 * @param path  The path object
 */
extern void   path_mkdir(path_t** path);

/** Iterate children of a directory pointed to by a path object.
 * @param path  The path object to iterate through
 * @return   An iterator object to be passed to "path_iter_next" and "path_iter_end"
 */
extern path_iterator_t* path_iter(path_t** path);

/** Get the next child of an iterator object.
 * @param iterator  The iterator object to iterate.
 * @return  A path representing the child. This path is invalidated on the next function call using this iterator.
 */
extern path_t* path_iter_next(path_iterator_t** iterator);

/** Finish iterating.
 * @param iterator  The iterator object to iterate.
 */
extern void path_iter_end(path_iterator_t** iterator);

#endif /* INCLUDE_UTIL_PATHBUF_H */
