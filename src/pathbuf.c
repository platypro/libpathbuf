/* Aeden McClain (c) 2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * license: MIT (see LICENSE.txt)
 *
 * This file is a part of libpathbuf.
 */

#include "pathbuf.h"

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

typedef struct path_header_t_ {
  path_type_t type;

  size_t len;
  size_t maxlen;

  /* Path flags */
  unsigned int flags;

  /* Iterator Fields. */
#ifdef _WIN32
  WIN32_FIND_DATA dir;
  HANDLE d;
  uint32_t state;
#else
  DIR *d;
  struct dirent *dir;
#endif

} path_header_t;

path_t* path_new(path_type_t type, size_t alloc, unsigned int flags);
void path_push_char(path_t** path, char c);
path_header_t* path_realloc(path_t** path);

/*
#define GET_PATH_FROM_HEADER(hdr)    ((path_t*)((char*)(hdr) + sizeof(path_header_t)))
#define GET_HEADER_FROM_PATH(path)   ((path_header_t*)((char*)(path) - sizeof(path_header_t)))

#define PATH(path) ((char*)(path))
#define PATH_AT(path, at) *(PATH(path) + at)
*/

#define GET_PATH_FROM_HEADER(hdr)    ((path_t*)(hdr))
#define GET_HEADER_FROM_PATH(path)   ((path_header_t*)(path))

#define PATH(path) ((char*)(path + 1))
#define PATH_AT(path, at) *(PATH(path) + at)

path_header_t* path_realloc(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);

  hdr->maxlen += PATH_DEFAULT_ALLOC;
  hdr = realloc(hdr, hdr->maxlen + sizeof(path_header_t));
  *path = GET_PATH_FROM_HEADER(hdr);

  return hdr;
}

void path_push_char(path_t** path, char c)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);

  hdr->len ++;

  if(hdr->len == hdr->maxlen)
    { hdr = path_realloc(path); }

  PATH_AT(*path, hdr->len - 1) = c;
  PATH_AT(*path, hdr->len) = '\000';
}

path_t* path_new(path_type_t type, size_t alloc, unsigned int flags)
{
  path_header_t* hdr = calloc(1, sizeof(path_header_t) + alloc);
  hdr->type = type;
  hdr->len = 0;
  hdr->maxlen = alloc;
  hdr->flags = flags;
  return GET_PATH_FROM_HEADER(hdr);
}

path_t* path_new_from_cwd(unsigned int flags)
{
  #ifdef _WIN32
  DWORD cwdlen = GetCurrentDirectory(0, NULL);
  path_t* result = path_new(PATH_TYPE_WINDOWS, cwdlen, flags);
  path_header_t* hdr = GET_HEADER_FROM_PATH(result);
  hdr->len = GetCurrentDirectory(hdr->maxlen, PATH(result));
  #else
  path_t* result = path_new(PATH_TYPE_UNIX, PATH_DEFAULT_ALLOC, flags);
  path_header_t* hdr = GET_HEADER_FROM_PATH(result);
  while(!getcwd(PATH(result), hdr->maxlen))
    { hdr = path_realloc(&result); }
  hdr->len = strlen(PATH(result));
  #endif
  return result;
}

static char path_get_delimiter(path_type_t type)
{
  switch(type)
  {
    case PATH_TYPE_WINDOWS_RELATIVE:
    case PATH_TYPE_WINDOWS:
      return '\\';
    case PATH_TYPE_UNIX_RELATIVE:
    case PATH_TYPE_UNIX:
      return '/';
  }
  return '/';
}

static bool path_is_updir(const char* str, const char slash)
{
  return (str[0] == '.' && str[1] == '.' && (str[2] == slash || str[2] == '\000'));
}

static bool path_is_samedir(const char* str, const char slash)
{
  return (str[0] == '.' && (str[1] == slash || str[1] == '\000'));
}

path_t* path_new_from_str(const char* src, unsigned int flags)
{
  /* Determine Path type */
  path_t* result = NULL;
  if(((src[0] != '\000') && (src[1] == ':') && (src[2] == '\\')) || (src[0] == '\\'))
    { result = path_new(PATH_TYPE_WINDOWS, PATH_DEFAULT_ALLOC, flags); }
  else if(src[0] == '/')
    { result = path_new(PATH_TYPE_UNIX, PATH_DEFAULT_ALLOC, flags); }
  else 
  {
    const char* tmp = src;
    while(*tmp)
    {
      if(*tmp == '/') 
      {
        result = path_new(PATH_TYPE_UNIX_RELATIVE, PATH_DEFAULT_ALLOC, flags); 
        break;
      }
      else if(*tmp == '\\')
      { 
        result = path_new(PATH_TYPE_WINDOWS_RELATIVE, PATH_DEFAULT_ALLOC, flags); 
        break;
      }
      else
        { tmp++; }
    }
    if(!result)
    {
      /* Ambiguous, pick according to platform. */
      #ifdef _WIN32
      result = path_new(PATH_TYPE_WINDOWS_RELATIVE, PATH_DEFAULT_ALLOC, flags);
      #else
      result = path_new(PATH_TYPE_UNIX_RELATIVE, PATH_DEFAULT_ALLOC, flags);
      #endif
    }
  }
  
  path_header_t* hdr = GET_HEADER_FROM_PATH(result);
  char slash = path_get_delimiter(hdr->type);

  switch(hdr->type)
  {
    case PATH_TYPE_WINDOWS:
      if(src[0] != slash) /* There is a drive letter */
      {
        path_push_char(&result, src[0]);
        path_push_char(&result, ':');
        src+=2;
      }
      path_push_char(&result, slash);
      src++;
      break;
    case PATH_TYPE_UNIX:
      path_push_char(&result, slash);
      break;
    case PATH_TYPE_UNIX_RELATIVE:
    case PATH_TYPE_WINDOWS_RELATIVE:
      path_push_char(&result, '.');
      break;
  }

  while(*src)
  {
    /* "..", go up. */
    if(path_is_updir(src, slash))
    {
      if(hdr->flags & PATH_FLAG_NO_PARENT)
      {
        /* Walk past root if applicable. */
        if(!strcmp(PATH(result), ".") || path_is_updir(&PATH_AT(result, hdr->len - 2), slash))
          { path_push(&result, src); }
        else
          { path_pop(&result); }
        src += 2;
      } else
        { src += 1; } // Go nowhere if up is disabled
    }
    /* ".", go nowhere. */
    else if (path_is_samedir(src, slash))
      { src += 1; }
    /* go down. */
    else
      { src = path_push(&result, src); }

    /* Skip slashes in src */
    while(*src && *src == slash)
      { src ++; }
  }

  if(hdr->len == 0)
  { 
    /* If empty, it is a relative root. */
    path_push_char(&result, '.');
  }

  return result;
}

const char* path_push(path_t** path, const char* subpath)
{
  if(!(*path)) return NULL;
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  char slash = path_get_delimiter(hdr->type);

  // Ignore if going up a directory
  if((hdr->flags & PATH_FLAG_NO_PARENT) && path_is_updir(subpath, path_get_delimiter(hdr->type)))
    { subpath += 2; }
  else
  {
    /* Special case for relative root. */
    if(PATH_AT(*path, 0) == '.' && PATH_AT(*path, 1) == '\000')
    {
      hdr->len --;
      *PATH(*path) = '\000';
    }

    /* Write slash if needed */
    if(hdr->len && PATH_AT(*path, hdr->len - 1) != slash)
    {
      path_push_char(path, slash);
    }

    /* Copy over path name */
    while(*subpath && *subpath != '/' && *subpath != '\\')
    {
      path_push_char(path, *subpath);
      subpath ++;
    }
  }

  /* Skip slashes in subpath */
  while(*subpath && (*subpath == '/' || *subpath == '\\'))
    { subpath ++; }

  path_push_char(path, '\000');
  GET_HEADER_FROM_PATH(*path)->len --;

  return(subpath);
}

void path_cat(path_t** path, const char* txt)
{
  while(*txt)
  { 
    path_push_char(path, *txt); 
    txt ++;
  }
}

void path_pop(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  char slash = path_get_delimiter(hdr->type);
  if(!(*path) || path_is_root(path)) return;

  char* walk = &PATH_AT(*path, hdr->len - 1);

  /* Skip path. */
  while(hdr->len && *walk != slash)
    { hdr->len --; walk --; }

  /* exit if root, otherwise continue. */
  PATH_AT(*path, hdr->len) = '\000';
  if(!path_is_root(path))
  {
    /* Skip last slash and end (absolute path) */
    if(hdr->len)
    {
      hdr->len --; walk --;
      PATH_AT(*path, hdr->len) = '\000';
    }
    else /* Replace with root (relative path) */
      { path_push_char(path, '.'); }
  }
}

extern char* path_extension_get(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  char* walk = &PATH_AT(*path, hdr->len - 1);
  
  if(*walk == '.') /* Not Ending with '.' */
    { return NULL; }

  while(walk != PATH(*path) && *walk != '.' && *walk != '/' && *walk != '\\')
    { walk --; }

  if((walk != PATH(*path)) && (*walk == '.') && (walk[-1] != '/') && (walk[-1] != '\\'))
    { return (walk + 1); }

  return NULL;
}

extern void path_extension_set(path_t** path, const char* extension)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);

  /* Remove extension if exists, leave dot. */
  char* ext = path_extension_get(path);
  if(ext)
    { hdr->len -= strlen(ext); }
  
  if(extension)
  {
    /* Add dot if not there. */
    if(!ext)
      { path_push_char(path, '.'); }

    /* Copy extension in. */
    while(*extension)
    {
      path_push_char(path, *extension);
      extension++;
    }
  } 
  else
  { 
    /* Remove dot. */
    if(ext)
    {
      hdr->len --;
      PATH_AT(*path, hdr->len) = '\000';
    }
  }
}

void path_destroy(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  free(hdr);
  *path = NULL;
}

char* path_spawn(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  char* result = malloc(hdr->len + 1);
  memcpy(result, PATH(*path), hdr->len + 1);
  return result;
}

extern char* path_release(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  size_t len = hdr->len + 1;
  memmove(hdr, PATH(*path), len);
  return ((char*) realloc(hdr, len));
}

void path_make_absolute(
    path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(path);
  if(hdr->type == PATH_TYPE_UNIX_RELATIVE || hdr->type == PATH_TYPE_WINDOWS_RELATIVE)
  {
    char* old = path_release(path);
    const char* old_walk = old;
    *path = path_new_from_cwd(0);
    while(old_walk)
      { old_walk = path_push(path, old_walk); }
    free(old);
  }
}

bool path_compare(path_t* p1, path_t* p2)
{
  return(!strcmp(PATH(p1), PATH(p2)));
}

path_t* path_clone(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  path_header_t* newhdr = malloc(sizeof(path_header_t) + hdr->maxlen);
  newhdr->len = hdr->len;
  newhdr->maxlen = hdr->maxlen;
  newhdr->type = hdr->type;

  path_t* result = GET_PATH_FROM_HEADER(newhdr);
  memcpy(PATH(result), PATH(*path), hdr->len + 1);

  return result;
}

bool path_exists(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  if(hdr->type == PATH_TYPE_WINDOWS || hdr->type == PATH_TYPE_WINDOWS_RELATIVE)
  {
#ifdef _WIN32
    return(PathFileExistsA(PATH(*path)));
#else
    return false;
#endif
  }
  else {
#ifndef _WIN32
    return(!access(PATH(*path), F_OK));
#else
    return false;
#endif
  }
}

char* path_get_cstr(path_t** path)
{
  return(PATH(*path));
}

char* path_get_basename(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  char* walk = &PATH_AT(*path, hdr->len - 1);
  char slash = path_get_delimiter(hdr->type);

  while(walk != PATH(*path) && *walk != slash)
    { walk --; }

  if(*walk == slash)
    { return walk + 1; }
  else
    { return walk; }
}

bool path_is_root(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  char* pathstr = PATH(*path);

  switch(hdr->type)
  {
    case PATH_TYPE_WINDOWS_RELATIVE:
    case PATH_TYPE_UNIX_RELATIVE:
      return (!strcmp(pathstr, "."));
    case PATH_TYPE_WINDOWS:
      return ((*pathstr && !strcmp(pathstr + 1, ":\\")) || (!strcmp(pathstr, "\\")));
    case PATH_TYPE_UNIX:
      return (!strcmp(pathstr, "/"));
  }

  return(false);
}

void path_mkdir(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  if(hdr->type == PATH_TYPE_WINDOWS || hdr->type == PATH_TYPE_WINDOWS_RELATIVE)
  {
#ifdef _WIN32
    _mkdir(PATH(*path));
#endif
  }
  else {
#ifndef _WIN32
    mkdir(PATH(*path), 0777);
#endif
  }
}

bool path_is_dir(path_t** path)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*path);
  if(hdr->type == PATH_TYPE_WINDOWS || hdr->type == PATH_TYPE_WINDOWS_RELATIVE)
  {
#ifdef _WIN32
    DWORD dwAttrib = GetFileAttributes(PATH(*path));

    return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
        (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
#else
    return false;
#endif
  }
  else
  {
#ifdef _WIN32
    return false;
#else
    struct stat s;
    if(!stat(PATH(*path), &s) && S_ISDIR(s.st_mode))
      { return true; }
#endif
  }

  return false;
}

path_iterator_t* path_iter(path_t** path)
{
  if(!path_is_dir(path)) { return NULL; }

  path_iterator_t* iter = path_clone(path);
  path_header_t* hdr = GET_HEADER_FROM_PATH(iter);

  if(hdr->type == PATH_TYPE_WINDOWS || hdr->type == PATH_TYPE_WINDOWS_RELATIVE)
  {
#ifdef _WIN32
    hdr->state = 0;
    return iter;
#else
    return NULL;
#endif
  }
  else
  {
#ifdef _WIN32
    return NULL;
#else
    hdr->dir = NULL;
    hdr->d = opendir(PATH(*path));
#endif
  }
  return iter;
}

#ifdef _WIN32
WIN32_FIND_DATA path_win_readdir(path_iterator_t* iter, HANDLE* d)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(iter);
  WIN32_FIND_DATA result = {0};
  if(hdr->state == 0)
  {
    path_push(&iter, "*");
    *d = FindFirstFile(PATH(iter), &result);
    path_pop(&iter);
    hdr->state = 1;
  }
  else if(hdr->state == 1) {
    if(!FindNextFileA(*d, &result))
      { hdr->state = 2; }
  }

  return result;
}
#endif

path_t* path_iter_next(path_iterator_t** iter)
{
  path_header_t* hdr = GET_HEADER_FROM_PATH(*iter);
  char slash = path_get_delimiter(hdr->type);
  if(hdr->type == PATH_TYPE_WINDOWS || hdr->type == PATH_TYPE_WINDOWS_RELATIVE)
  {
#ifdef _WIN32
    if(hdr->state < 2)
    {
      if(hdr->state == 1)
        { path_pop(iter); }

      do {
        hdr->dir = path_win_readdir(*iter, &hdr->d);
      } while(path_is_samedir(hdr->dir.cFileName, slash)
           || path_is_updir(hdr->dir.cFileName, slash));
    }
    
    /* Recheck state */  
    if(hdr->state < 2)
      { path_push(iter, hdr->dir.cFileName); }
    else
      { return NULL; }
#else
    return NULL;
#endif
  }
  else
  {
#ifdef _WIN32
    return NULL;
#else
    if(hdr->d)
    {
      if(hdr->dir)
        { path_pop(iter); }

      do {
        hdr->dir = readdir(hdr->d);
        if(!hdr->dir)
        {
          closedir(hdr->d);
          hdr->d = NULL;
          return NULL;
        }
      } while(path_is_samedir(hdr->dir->d_name, slash)
           || path_is_updir(hdr->dir->d_name, slash));

      path_push(iter, hdr->dir->d_name);
    }
    else
      { return NULL; }
#endif
  }
  return *iter;
}

void path_iter_end(path_iterator_t** iter)
{
#ifndef _WIN32
  path_header_t* hdr = GET_HEADER_FROM_PATH(*iter);
  if(hdr->d)
    { closedir(hdr->d); }
#endif
  path_destroy(iter);
}
