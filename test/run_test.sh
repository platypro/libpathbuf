#! /bin/bash

# Make dummy files
mkdir -p dir/subdir
mkdir -p dir/subdir2
touch dir/text.txt

# Run tests
./pathbuf_tester $PWD

# Cleanup
rm -r dir
