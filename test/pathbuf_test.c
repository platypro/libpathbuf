/* Aeden McClain (c) 2020
 * web:   https://www.platypro.net
 * email: dev@platypro.net
 * license: MIT (see LICENSE.txt)
 *
 * This file is a part of libpathbuf.
 */

#include <pathbuf.h>

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#ifdef _WIN32
#define COLOUR_TITLE ""
#define COLOUR_ERROR "  "
#define COLOUR_NORMAL ""
#else
#define COLOUR_TITLE "\033[1;32m"
#define COLOUR_ERROR "  \033[1;31m"
#define COLOUR_NORMAL "\033[0m"
#endif

typedef struct test
{
  char* name;
  char* funName;
  void (*fun)();
} TEST;

#define MKTEST(name, fun) {name, #fun, fun}

char *cwd = NULL;

uint32_t error = 0;

bool assert_ptr(const void* actual,const void* expected)
{
  bool result = (actual == expected);
  if(!result)
  {
    error ++;
    printf(COLOUR_ERROR "Expected %p, got %p\n", expected, actual);
  }
  return result;
}

bool assert_str(const char* actual, const char* expected)
{
  if(!actual || !expected) return assert_ptr(actual, expected);
  bool result = !strcmp(actual, expected);

  if(!result)
  {
    error ++;
    printf(COLOUR_ERROR "Expected \"%s\", Actually got \"%s\"\n", expected, actual);
  }
  return result;
}

bool assert_compare_path(path_t* actual, const char* expected)
{
  return assert_str(path_get_cstr(&actual), expected);
}

bool assert_bool(bool actual, bool expected, char* memo)
{
  bool result = (actual == expected);
  if(!result)
  {
    error ++;
    printf(COLOUR_ERROR "Expected %s, got %s (%s)\n", expected ? "True" : "False", actual ? "True" : "False", memo);
  }
  return result;
}

void test_path_push()
{
  char* path_orig[4] = {
    "/asdf/asdf", "asdf/asdf",
    "c:\\asdf\\asdf", "asdf\\asdf"};

  char* path_new[4] = {
    "/asdf/asdf/Hello", "asdf/asdf/Hello",
    "c:\\asdf\\asdf\\Hello", "asdf\\asdf\\Hello"};

  for(int i = 0; i < 4; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    path_push(&path, "Hello");
    printf(COLOUR_NORMAL "  Push 'Hello' to '%s'\n", path_orig[i]);
    assert_compare_path(path, path_new[i]);
    path_destroy(&path);
  }
}

void test_path_pop()
{
  char* path_orig[10] = {
    "/asdf/asdf", "asdf/asdf",
    "c:\\asdf\\asdf", "asdf\\asdf",
    "c:\\asdf", "/asdf", "asdf",
    "/", ".", "c:\\"};

  char* path_new[10] = {
    "/asdf", "asdf",
    "c:\\asdf", "asdf",
    "c:\\", "/", ".",
    "/", ".", "c:\\"};

  for(int i = 0; i < 10; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    path_pop(&path);
    printf(COLOUR_NORMAL "  Popping '%s' once\n", path_orig[i]);
    assert_compare_path(path, path_new[i]);
    path_destroy(&path);
  }
}

void test_path_new_from_cwd()
{
  if(!cwd) return;
  path_t* path = path_new_from_cwd();
  assert_compare_path(path, cwd);
  path_destroy(&path);
}

void test_path_new_from_str()
{
  char* path_orig[7] = {
    "/asdf/./aaa/../bbb", "asdf/./aaa/../../../bbb",
    "c:\\asdf\\.\\aaa\\..\\bbb", "asdf\\.\\aaa\\..\\..\\..\\..\\bbb",
    ".", "/", "\\"};

  char* path_new[7] = {
    "/asdf/bbb", "../bbb",
    "c:\\asdf\\bbb", "..\\..\\bbb",
    ".", "/", "\\"};

  for(int i = 0; i < 7; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    assert_compare_path(path, path_new[i]);
    path_destroy(&path);
  }
}

void test_path_clone()
{
  path_t* path1 = path_new_from_str("testpath");
  path_t* path2 = path_clone(&path1);
  path_destroy(&path1);
  assert_compare_path(path2, "testpath");
  path_destroy(&path2);
}

void test_path_is_root()
{
  char* path_orig[4] = {
    "/", ".", "c:\\", "\\"};

  for(int i = 0; i < 4; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    assert_bool(path_is_root(&path), true, path_orig[i]);
    path_destroy(&path);
  }
}

void test_path_is_not_root()
{
  char* path_orig[4] = {
    "/aaa", "../aaa",
    "c:\\aaa", "..\\aaa"};

  for(int i = 0; i < 4; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    assert_bool(path_is_root(&path), false, path_orig[i]);
    path_destroy(&path);
  }
}

void test_path_get_basename()
{
  char* path_orig[5] = {
    "/aaa", "../ffe/bbb",
    "c:\\aaa\\ccc", "..\\ddd", "eee"};

  char* path_new[5] = {
    "aaa", "bbb",
    "ccc", "ddd", "eee"};

  for(int i = 0; i < 5; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    assert_str(path_get_basename(&path), path_new[i]);
    path_destroy(&path);
  }
}

void test_path_exists()
{
  path_t* path = NULL;

#ifdef _WIN32
  path = path_new_from_str("dir\\subdir");
  assert_bool(path_exists(&path), true, "dir\\subdir");
  path_destroy(&path); 
#else
  path = path_new_from_str("dir/subdir");
  assert_bool(path_exists(&path), true, "dir/subdir");
  path_destroy(&path);
#endif

  path = path_new_from_cwd();
  path_push(&path, "dir");
  path_push(&path, "subdir");
  assert_bool(path_exists(&path), true, "$PWD/dir/subdir");
  path_destroy(&path);
}

void test_path_not_exists()
{
  char* path_orig[4] = {
    "/nonexistingfile.txt", "nonexistingfile.txt",
    "c:\\nonexistingfile.txt", "nonexistingfile.txt"};

  for(int i = 0; i < 4; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    assert_bool(path_exists(&path), false, path_orig[i]);
    path_destroy(&path);
  }
}

void test_path_is_dir()
{
  path_t* path = NULL;

  path = path_new_from_cwd();
  path_push(&path, "dir");
  path_push(&path, "subdir");
  assert_bool(path_is_dir(&path), true, "$PWD/dir/subdir");
  path_destroy(&path);

#ifdef _WIN32
  path = path_new_from_str("dir\\subdir");
  assert_bool(path_is_dir(&path), true, "dir\\subdir");
  path_destroy(&path);
#else
  path = path_new_from_str("dir/subdir");
  assert_bool(path_is_dir(&path), true, "dir/subdir");
  path_destroy(&path);
#endif
}

void test_path_is_not_dir()
{
  path_t* path;

  path = path_new_from_cwd();
  path_push(&path, "dir");
  path_push(&path, "text.txt");
  assert_bool(path_is_dir(&path), false, "$PWD/dir/text.txt");
  path_destroy(&path);

#ifdef _WIN32
  path = path_new_from_str("dir\\text.txt");
  assert_bool(path_is_dir(&path), false, "dir\\text.txt");
  path_destroy(&path);
#else
  path = path_new_from_str("dir/text.txt");
  assert_bool(path_is_dir(&path), false, "dir/text.txt");
  path_destroy(&path);
#endif
}

void test_path_iter_do(path_t* path)
{
  uint32_t checks = 0;
  path_t* subpath = NULL;
  path_t* subpath_test = path_clone(&path);
  path_iterator_t* iter = path_iter(&path);

  if(!iter)
  {
    printf(COLOUR_NORMAL "Test dir does not exist, skipping...\n");
    return;
  }

  for(int i = 3; i; i--)
  {
    subpath = path_iter_next(iter);
    if(!subpath) { break; }

    path_push(&subpath_test, "subdir");
    if(!strcmp(path_get_cstr(&subpath), path_get_cstr(&subpath_test)))
      { checks ^= 1; }
    path_pop(&subpath_test);

    path_push(&subpath_test, "subdir2");
    if(!strcmp(path_get_cstr(&subpath), path_get_cstr(&subpath_test)))
      { checks ^= 2; }
    path_pop(&subpath_test);

    path_push(&subpath_test, "text.txt");
    if(!strcmp(path_get_cstr(&subpath), path_get_cstr(&subpath_test)))
      { checks ^= 4; }
    path_pop(&subpath_test);
  }

  subpath = path_iter_next(iter);
  assert_ptr(subpath, NULL);

  path_destroy(&subpath_test);
  path_iter_end(iter);

  if(~checks & 1)
  {
    error ++;
    printf(COLOUR_ERROR "Could not find \"subdir\"\n");
  }

  if(~checks & 2)
  {
    error ++;
    printf(COLOUR_ERROR "Could not find \"subdir2\"\n");
  }

  if(~checks & 4)
  {
    error ++;
    printf(COLOUR_ERROR "Could not find \"text.txt\"\n");
  }
}

void test_path_iter()
{
  path_t* path = NULL;
  
  path = path_new_from_cwd();
  path_push(&path, "dir");
  test_path_iter_do(path);
  path_destroy(&path);

  path = path_new_from_str("dir");
  test_path_iter_do(path);
  path_destroy(&path);
}

void test_path_extension_get()
{
  char* path_orig[7] = {
    "/asdf/dsdd.ggg/dfs.txt", "asdf/dsdd.ggg/dfs.txt",
    "c:\\asdf\\dsdd.ggg\\dfs.txt", "asdf\\dsdd.ggg\\dfs.txt",
    "c:\\.hidden", "c:\\asd.asdf\\asdf", "c:\\asdf\\asdf."};

  char* path_new[7] = {"txt", "txt", "txt", "txt", NULL, NULL, NULL};

  for(int i = 0; i < 7; i++)
  {
    path_t* path = path_new_from_str(path_orig[i]);
    assert_str(path_extension_get(&path), path_new[i]);
    path_destroy(&path);
  }
}

void test_path_extension_set()
{
  char* path_orig[4] = {
    "/asdf/dsdd.ggg/dfs", "asdf/dsdd.ggg/dfs",
    "c:\\asdf\\dsdd.ggg\\dfs", "asdf\\dsdd.ggg\\dfs"};

  char* path_new[4] = {"/asdf/dsdd.ggg/dfs.txt", "asdf/dsdd.ggg/dfs.txt",
    "c:\\asdf\\dsdd.ggg\\dfs.txt", "asdf\\dsdd.ggg\\dfs.txt"};

  path_t* path = NULL;

  for(int i = 0; i < 4; i++)
  {
    path = path_new_from_str(path_orig[i]);

    path_extension_set(&path, "txt");
    assert_compare_path(path, path_new[i]);

    path_extension_set(&path, NULL);
    assert_compare_path(path, path_orig[i]);

    path_destroy(&path);
  }

  path = path_new_from_str("c:\\asdf\\dsdd.ggg\\dfs");
  path_extension_set(&path, NULL);
  assert_compare_path(path, "c:\\asdf\\dsdd.ggg\\dfs");
  path_destroy(&path);
}

void test_path_spawn()
{
  path_t* path = path_new_from_str("/dir1/dir2");
  char* spawn = path_spawn(&path);
  path_push(&path, "dir3");

  assert_str(spawn, "/dir1/dir2");
  assert_compare_path(path, "/dir1/dir2/dir3");

  path_destroy(&path);
  free(spawn);
}

void test_path_release()
{
  path_t* path = path_new_from_str("/dir1/dir2");
  char* spawn = path_release(&path);

  assert_str(spawn, "/dir1/dir2");

  free(spawn);
}

void test_path_cat()
{
  path_t* path = path_new_from_str("/dir1/dir2");
  path_cat(&path, "asdf");

  assert_compare_path(path, "/dir1/dir2asdf");

  path_destroy(&path);
}

TEST tests[] =
{
  MKTEST("New from string", test_path_new_from_str),
  MKTEST("Walk down the tree", test_path_push),
  MKTEST("Walk up the tree", test_path_pop),
  MKTEST("Path is root", test_path_is_root),
  MKTEST("Path is not root", test_path_is_not_root),
  MKTEST("Get Basename", test_path_get_basename),
  MKTEST("Get Extension", test_path_extension_get),
  MKTEST("Set Extension", test_path_extension_set),
  MKTEST("Path cat", test_path_cat),
  MKTEST("Check existence", test_path_exists),
  MKTEST("Check non-existence", test_path_not_exists),
  MKTEST("Check if dir", test_path_is_dir),
  MKTEST("Check if not dir", test_path_is_not_dir),
  MKTEST("Iterate Directory", test_path_iter),
  MKTEST("New from cwd", test_path_new_from_cwd),
  MKTEST("Clone a path object", test_path_clone),
  MKTEST("Spawn a path", test_path_spawn),
  MKTEST("Release a path", test_path_release),
};

#define NUM_TESTS (uint32_t)(sizeof(tests) / sizeof(TEST))

int main(int argc, char** argv)
{
  if(argc > 1)
    { cwd = argv[1]; }

  for(int i = 0; i < NUM_TESTS; i++)
  {
    TEST test = tests[i];
    printf(COLOUR_TITLE "(%.2u/%.u) Testing \"%s\" (%s)\n", i + 1, NUM_TESTS, test.name, test.funName);
    test.fun();
  }

  if(error == 0)
    { puts(COLOUR_TITLE "All tests successful!"); }
  else
  {
    printf(COLOUR_TITLE "Tests failed with %d errors\n", error);
  }

  return 0;
}
